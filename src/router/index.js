import Vue from 'vue';
import VueRouter from 'vue-router';
import LetItSnow from 'vue-let-it-snow';
import Home from '../views/Home.vue';
import CadeauUn from '../views/Cadeau_un.vue';
import CadeauDeux from '../views/Cadeau_deux.vue';
import CadeauTrois from '../views/Cadeau_trois.vue';
import CadeauFinal from '../views/Cadeau_final.vue';

Vue.use(LetItSnow);

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      clicked: {
        value: false,
      },
      title: {
        value: 'Joyeux Noël',
      },
    },
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/cadeau/1',
    name: 'CadeauUn',
    component: CadeauUn,
    meta: {
      clicked: false,
      title: {
        value: 'Petit poème',
      },
    },
  },
  {
    path: '/cadeau/2',
    name: 'CadeauDeux',
    component: CadeauDeux,
    meta: {
      title: {
        value: 'Petit dessin',
      },
    },
  },
  {
    path: '/cadeau/3',
    name: 'CadeauTrois',
    component: CadeauTrois,
    meta: {
      title: {
        value: 'Petite chanson',
      },
    },
  },
  {
    path: '/cadeau/final',
    name: 'CadeauFinal',
    component: CadeauFinal,
    meta: {
      title: {
        value: 'Le véritable kdo',
      },
    },
  },
];

const router = new VueRouter({
  routes,
});

export default router;
