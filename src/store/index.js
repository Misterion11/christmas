import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    kdoClicked: [],
  },
  mutations: {
    ADD_FIGCAPTION(s, payload) {
      s.kdoClicked.push(payload);
    },
  },
  actions: {
    addFigcaption(context, payload) {
      context.commit('ADD_FIGCAPTION', payload);
    },
  },
  getters: {

  },
});

export default store;
